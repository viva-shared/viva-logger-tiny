//@ts-check
const lib_logger = require('./app.js') //or 'viva-logger-tiny', if see this example from npm
let logger = new lib_logger(__dirname, {file_prefix: 'my_best_app'})

logger.debug('test1')
logger.debug('test2', 'details')
logger.error('test3', new Error('this is error'))
logger.debug('test4')
setTimeout(() => {
    logger.error('test5')
}, 5000)