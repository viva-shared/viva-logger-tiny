//@ts-check

const lib_os = require('os')
const lib_fs = require('fs-extra')
const lib_path = require('path')
const lib_convert = require('viva-convert')
const app = require('./app.js')

/**
 * @private
 * @typedef type_log_buffer
 * @property {string} info
 * @property {'error'|'debug'} type
 * @property {string|Error} details
 * @property {Date} d
 * @property {boolean} writed
 */

/**
 * @typedef type_env_ext
 * @property {string} path
 * @property {type_log_buffer[]} log_buffer
 */

/**
 * @typedef {app.type_options & type_env_ext} type_env
 */

/** @type {type_env} */
let env = {
    path: __dirname,
    days_life: 3,
    file_prefix: 'app',
    write_to_console: true,
    log_buffer: []
}

/** @type {NodeJS.Timer} */
let timer_delete_old_log_files = undefined
/** @type {NodeJS.Timer} */
let timer_write_from_log_buffer = undefined

exports.env = env
exports.get_file_name = get_file_name
exports.format_message = format_message
exports.start_timer_delete_old_log_files = start_timer_delete_old_log_files
exports.start_timer_write_from_log_buffer = start_timer_write_from_log_buffer

/**
 * @param {Date} date
 * @returns {string}
 */
function get_file_name(date) {
    return lib_path.join(env.file_prefix.concat('_', lib_convert.formatDate(date, 112), '.log'))
}

/**
 * @param {'error'|'debug'} type
 * @param {string} info
 * @param {string|Error} details
 * @param {Date} d
 */
function format_message(type, info, details, d) {
    /** @type {string[]} */
    let message = []

    let type_string = ''
    if (type === 'error') {
        type_string = 'ERROR'
    } else if (type === 'debug') {
        type_string = 'DEBUG'
    }
    if (lib_convert.isEmpty(type_string)) return

    message.push(lib_convert.format("--->{0} {1} {2}", [lib_convert.formatDate(d,1041083), type_string, lib_convert.toString(info, '').trim()]))
    if (!lib_convert.isEmpty(details)) {
        if (typeof details === 'string') {
            message.push(lib_convert.toString(details, '').trim())
        } else {
            message.push(lib_convert.toErrorMessage(details).trim())
        }
        message.push('')
    }

    return message.join(lib_os.EOL)
}

function start_timer_delete_old_log_files() {
    if (!lib_convert.isAbsent(timer_delete_old_log_files)) {
        clearTimeout(timer_delete_old_log_files)
        timer_delete_old_log_files = undefined
    }

    let start_timeout_delete_old_files = 1000 * 5 // 5 sec
    let continue_timeout_delete_old_files = 1000 * 60 * 60 * 3 // 3 hour

    timer_delete_old_log_files = setTimeout(function tick() {
        let now = Date()

        /** @type {string[]} */
        let undeletable_files = []
        for (let i = 0; i < env.days_life; i++) {
            undeletable_files.push(get_file_name(lib_convert.dateAdd('day', -1 * i, now)))
        }

        lib_fs.pathExists(env.path, (error, exists) => {
            if (!lib_convert.isEmpty(error)) {
                console.error(lib_convert.format('Error in library "viva-logger-tine" when check exists path "{0}":{1}{2}', [env.path, lib_os.EOL, error]))
                return
            }
            if (exists !== true) {
                return
            }
            lib_fs.readdir(env.path, (error, files) => {
                if (!lib_convert.isEmpty(error)) {
                    console.error(lib_convert.format('Error in library "viva-logger-tine" when scan dir "{0}" for find old file log:{1}{2}', [env.path, lib_os.EOL, error]))
                    return
                }
                let log_file_mask = new RegExp(env.file_prefix.concat('_[0-9]{8}.log'))
                let old_log_files = files.filter(f => log_file_mask.test(f) === true && !undeletable_files.includes(f) && lib_convert.equal('.log', lib_path.extname(f)))
                old_log_files.forEach(file_for_delete => {
                    let full_file_for_delete = lib_path.join(env.path, file_for_delete)
                    lib_fs.unlink(full_file_for_delete, error => {
                        if (!lib_convert.isEmpty(error)) {
                            console.error(lib_convert.format('Error in library "viva-logger-tine" when delete old log file "{0}":{1}{2}', [full_file_for_delete, lib_os.EOL, error]))
                        }
                    })
                })
            })
        })

        timer_delete_old_log_files = setTimeout(tick, continue_timeout_delete_old_files)
    }, start_timeout_delete_old_files)
}

function start_timer_write_from_log_buffer() {
    if (!lib_convert.isAbsent(timer_write_from_log_buffer)) {
        clearTimeout(timer_write_from_log_buffer)
        timer_write_from_log_buffer = undefined
    }

    let timeout_write_from_log_buffer = 1000 // 1 sec
    timer_write_from_log_buffer = setTimeout(function tick() {

        /** @type {string[]} */
        let log_info = []

        /** @type {Date} */
        let d_first

        env.log_buffer.filter(f => f.writed !== true).forEach((log, log_index) => {
            if (log_index === 0) {
                d_first = lib_convert.toDateWithoutTime(log.d)
            }

            if (!lib_convert.isAbsent(d_first) && lib_convert.equal(lib_convert.formatDate(d_first, 112), lib_convert.formatDate(log.d, 112))) {
                log.writed = true
                log_info.push(format_message(log.type, log.info, log.details, log.d))
            }
        })

        env.log_buffer = env.log_buffer.filter(f => f.writed !== true)

        if (!lib_convert.isAbsent(d_first) && log_info.length > 0) {
            let full_file_log_name = lib_path.join(env.path, get_file_name(d_first))
            let data = log_info.join(lib_os.EOL).trim().concat(lib_os.EOL)
            lib_fs.appendFile(full_file_log_name, data, {encoding: 'utf8'}, error => {
                if (!lib_convert.isEmpty(error)) {
                    console.error(lib_convert.toErrorMessage(error, 'Error in library "viva-logger-tine" when write log to file "{0}":{1}{2}', [full_file_log_name, lib_os.EOL,  error]))
                }
                timer_write_from_log_buffer = setTimeout(tick, timeout_write_from_log_buffer)
            })
        } else {
            timer_write_from_log_buffer = setTimeout(tick, timeout_write_from_log_buffer)
        }
    }, timeout_write_from_log_buffer)
}