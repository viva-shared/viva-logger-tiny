//@ts-check

const lib_convert = require('viva-convert')
const _private = require('./app_private.js')

/**
 * logger settings
 * @typedef type_options
 * @property {string} [file_prefix] prefix log files, example - 'my_app'
 * @property {number} [days_life] number of days for which log files will be stored, default = 3, min = 1 (current date)
 * @property {boolean} [write_to_console] default = true
 */

class App {
    /**
     *
     * @param {string} path path for storage log file(s)
     * @param {type_options} [options]
     */
    constructor(path, options) {
        _private.env.path = lib_convert.toString(path, _private.env.path)
        if (!lib_convert.isAbsent(options)) {
            _private.env.file_prefix = lib_convert.toString(options.file_prefix, _private.env.file_prefix)
            _private.env.days_life = lib_convert.toInt(options.days_life, _private.env.days_life)
            if (_private.env.days_life < 1) {
                _private.env.days_life = 1
            }
            _private.env.write_to_console = lib_convert.toBool(options.write_to_console, _private.env.write_to_console)
        }

        _private.start_timer_delete_old_log_files()
        _private.start_timer_write_from_log_buffer()
    }

    /**
     * @param {string} info
     * @param {string|Error} [details]
     */
    debug(info, details) {
        /** @type {_private.type_log_buffer} */
        let log = {
            info: info,
            type: 'debug',
            details: details,
            d: new Date(),
            writed: false
        }
        if (_private.env.write_to_console === true) {
            console.log(_private.format_message(log.type, log.info, log.details, log.d))
        }
        _private.env.log_buffer.push(log)
    }

    /**
     * @param {string} info
     * @param {string|Error} [details]
     */
    error(info, details) {
        /** @type {_private.type_log_buffer} */
        let log = {
            info: info,
            type: 'error',
            details: details,
            d: new Date(),
            writed: false
        }
        if (_private.env.write_to_console === true) {
            console.error(_private.format_message(log.type, log.info, log.details, log.d))
        }
        _private.env.log_buffer.push(log)
    }
}

module.exports = App

